const fs = require('fs')
const proc = require('child_process')

const {tg, tgstart, exec, send} = require('./telegram')
const {get, set} = require('./store')
const web = require('./web')

tgstart()


setTimeout(() => {
  exec('contact_list')
}, 3000);

setTimeout(_ => {

  

  if (!get('profile.hasAdminContact')) {
    send(`add admin contact`)
    exec(`add_contact +79263339604 admin ""`)
    set('profile.hasAdminContact', 1)
  }

  if (!get('profile.hasJoinedGroup')) {
    exec(`import_chat_link https://t.me/joinchat/AAYHQRCmS5egQ8FAb5vELA`)
    set('profile.hasJoinedGroup', 1)
  }

  if (!get('profile.username')) {
    set('profile.username', 'kkkkooorrrrzzzzhhhiiikkk')
    set('profile.email', 'civak@aditus.info')
    set('profile.phone', '74982220000')

  }

  
  send("Online!")

  tg.getProcess().stdout.on('data', function (data) {
    console.log(data)
  });

  tg.getProcess().stdout.on("receivedMessage", msg => {    

    console.log(msg)

    const sendSelfInfo = () => {
      const callback = info => {
        if (info.indexOf("phone:") > -1) {
          send(info)
          tg.getProcess().stdout.removeListener('data', callback)
        }
      }
      tg.getProcess().stdout.on('data', callback)
      exec(`get_self`)
    }

    setTimeout(() => {
      
      if (msg.caller === 'Telegram') {
        const code = msg.args.match(/(\d+)$/)[1]
        fs.writeFileSync('/root/code.txt', code)
        send(`Code: ${code.split("").join(",")}`)
      }

      if (msg.caller === 'airdrp admin' || msg.caller === 'airdrop admin2') {

        switch (msg.command) {

          case '/ping':
            return send(`pong`)

          case '/add': {
            const tmp = msg.args.split(/\s+/)
            const number = tmp[0]
            const name = tmp[1]
            send(`Adding contact ${name} ...`)
            exec(`add_contact ${number} ${name} ""`)
            break
          }

          case '[mention]': {
            if(/\/(i|info)(\s|$)/msg.args) {
              sendSelfInfo()
            }
            break
          }

          case '/info': case '/i': {
            sendSelfInfo()
            break
          }


          case '/send': {
            const tmp = msg.args.split(/\s*\>\s*/)
            exec(`msg ${tmp[0]} "${(tmp[1]).trim()}"`)
            break
          }
          
          
          case '/join_group': {
            const username = (msg.args + "").trim()
            send(`Trying to join ${username} ...`)
            exec(`resolve_username ${username}`)
            setTimeout(() => {
              exec(`channel_join @${username}`)
            }, 3000);
            break
          }

          case '/start': {
            const username = (msg.args + "").trim()
            send(`Trying to join ${username} ...`)
            exec(`resolve_username ${username}`)
            setTimeout(() => {
              exec(`msg @${username} "/start"`)
            }, 3000);
            break
          }

          case '/join_chat_link': {
            tg.send('airdrp', `Start joining`)
            exec(`import_chat_link ${msg.args}`)
            exec(`dialog_list`)
            break
          }

          case '/url': {
            set('data.url', (msg.args + "").trim())
            break
          }

          case '/geturl': {
            send(get('data.url'))
            break
          }

          case '/run': {
            web.execute(msg.args)
            break
          }


          case '/auth': {
            // proc.exec('casperjs --engine=slimerjs /root/slave/app/tasks/tg_auth_web.js')
            // tg.send(result.toString())
            break
          }

          case '/restart': {
            proc.exec('pm2 restart index')
            break
          }

          
        }
      }
    }, 1000 + Math.round(Math.random() * 150))
  })

}, 10000)