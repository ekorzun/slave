const fs = require('fs')
const conf = require('./conf')
const Chance = require('chance')
const chance = new Chance


if (!fs.existsSync(`${conf.root}/exist.txt`)) {
  const SimpleTelegram = require('simple-telegram')
  const tg = new SimpleTelegram()
  tg.create('/bin/telegram-cli', '/etc/telegram-cli/server.pub', '-vv')
  const exec = cmd => {
    console.log('tg:', cmd)
    tg.getProcess().stdin.write(cmd + "\n", 'utf8')
  }
  const agent = require('superagent')

  const initReg = (phone, code) => {
    console.log('registering', phone, code)
    setTimeout(_ => {
      exec('Y')
      setTimeout(() => {
        exec(chance.first())
        setTimeout(() => {
          exec(chance.last())  
          setTimeout(() => {
            exec(code)
            setTimeout(() => {
              exec(`add_contact +79263339604 admin ""`)
              exec(`add_contact +79169192263 admin2 ""`)
              setTimeout(() => {
                exec(`import_chat_link https://t.me/joinchat/AAYHQRCmS5egQ8FAb5vELA`)
                setTimeout(() => {
                  fs.writeFileSync(`${conf.root}/exist.txt`, 'ok')
                  tg.quit()
                }, 5000);
              }, 5000);
            }, 5000);
          }, 5000);
        }, 5000);
      }, 5000);
    }, 5000)
  }


  // setTimeout(() => {
  //   initReg('79192415570', '56312')
  // }, 10000);

  setTimeout(_ => {
    const req = agent.get(conf.sim)
    req.query({action: 'getNumber',service: 'tg'}).end((err, res) => {
      if(err) {
        throw new Error('failed tg register')
      }
      console.log(res.text)
      // ACCESS_NUMBER:5199834:79192415570
      const resp = res.text.split(':')
      const id = resp[1]
      const phone = resp[2]
      if(resp.length === 3) {
        exec(phone)

        agent.get(conf.sim).query({ action: 'setStatus', status: 1, id: id}).end((err, res) => {
          if(err) {
            throw new Error('failed tg set status')
          }
          console.log(res.text)
          if (res.text === 'ACCESS_READY') {
            const check = () => {
              agent.get(conf.sim).query({ action: 'getStatus', id: id }).end((err, res) => {
                if(err) {
                  throw new Error('failed to check status')
                }
                console.log(res.text)
                if (res.text === 'STATUS_WAIT_CODE' || res.rext === 'ACCESS_READY') {
                  return setTimeout(check, 20000);
                }
                if (/STATUS_OK/.test(res.text)) {
                  const code = res.text.split(':')[1]
                  initReg(phone, code)
                }
              })
            }

            setTimeout(check, 10000);
          }
        })
      } else {
        throw new Error(res.text)
      }
    })

  }, 5000)
} else {
  console.log('Ok!')
}




// setTimeout(_ => {


//   const exec = cmd => tg.getProcess().stdin.write(cmd + "\n", 'utf8')
//   exec('+79263339604')

// }, 5000)