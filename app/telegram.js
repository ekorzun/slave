const SimpleTelegram = require('./lib/tg')
const tg = new SimpleTelegram()

const tgstart = () => tg.create('/bin/telegram-cli', '/etc/telegram-cli/server.pub')
const exec = cmd => tg.getProcess().stdin.write(cmd + "\n", 'utf8')

const send = x => {
  console.log(x)
  const cmd = `msg airdrp "${x.trim().split('"').join("\"").split("\r").join("\\r").split("\n").join("\\n")}"`
  exec(cmd)
}

module.exports = {
  tg,
  tgstart,
  exec,
  send
}