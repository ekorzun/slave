const fs = require('fs')
const path = require('path')
var casper = require('casper').create();
const dump = require('utils').dump

const x = + new Date
const check = () => fs.existsSync('/root/code.txt') && fs.readFileSync('/root/code.txt').toString()

casper.options.viewportSize = { width: 1598, height: 804 };
casper.options.waitTimeout = 130000
casper.options.stepTimeout = 130000

casper.on("remote.message", function(msg){
    this.echo(msg);
});

casper.on('page.error', function (msg, trace) {
  this.echo('Error: ' + msg, 'ERROR');
  for (var i = 0; i < trace.length; i++) {
    var step = trace[i];
    this.echo('   ' + step.file + ' (line ' + step.line + ')', 'ERROR');
  }
});

const phone = casper.cli.get("phone")

casper.start('http://localhost:8000/app/', function () {
  this.wait(5000)
  this.evaluate(function(){
    console.log( (new window.Date) + "" )
    setTimeout(() => {
      window.$scope.credentials.phone_country = '+7'
      window.$scope.credentials.phone_number = '9111489077'
      setTimeout(() => {
        window.$scope.sendCode()
        console.log(window.$scope.credentials.phone_number)
      }, 1000);
      
    }, 3000);
  })

  this.waitFor(function(){
    const code = check()
    if(code) {
      this.echo("CODE!!!!", code)
      return true
    }
  })
}).run();

// casper.test.begin('test', function (test) {
//   casper.start('http://localhost:8000/app/#/login');


//   casper.waitForSelector("form[name=mySendCodeForm] input[name='phone_country']",
//     function success() {
//       test.assertExists("form[name=mySendCodeForm] input[name='phone_country']");
//       this.click("form[name=mySendCodeForm] input[name='phone_country']");
//     },
//     function fail() {
//       test.assertExists("form[name=mySendCodeForm] input[name='phone_country']");
//     });

//   casper.waitForSelector("input[name='phone_country']",
//     function success() {
//       test.assertExists("input[name='phone_country']");
//       this.sendKeys("input[name='phone_country']", "7");
//     },
//     function fail() {
//       test.assertExists("input[name='phone_country']");
//     });

//   casper.waitForSelector("form[name=mySendCodeForm] input[name='phone_number']",
//     function success() {
//       test.assertExists("form[name=mySendCodeForm] input[name='phone_number']");
//       this.click("form[name=mySendCodeForm] input[name='phone_number']");
//     },
//     function fail() {
//       test.assertExists("form[name=mySendCodeForm] input[name='phone_number']");
//     });

//   casper.waitForSelector("input[name='phone_number']",
//     function success() {
//       test.assertExists("input[name='phone_number']");
//       this.sendKeys("input[name='phone_number']", phone);
//     },
//     function fail() {
//       test.assertExists("input[name='phone_number']");
//     });

//   casper.waitForSelector(".login_head_submit_btn",
//     function success() {
//       test.assertExists(".login_head_submit_btn");
//       this.click(".login_head_submit_btn");
//     },
//     function fail() {
//       test.assertExists(".login_head_submit_btn");
//     });

//   casper.capture('/root/.telegram-cli/a.png')


//   casper.wait(5000,
//     function success() {
//       this.mouse.click(680, 470)
//     })

//   casper.run(function () { test.done(); });
// });