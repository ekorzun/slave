const path = require('path')
const guid = require('guid')
const fs = require('fs')
const proc = require('child_process')
const { get, set } = require('./store')

const web = {}

web.value = txt => {
  if (txt === '1111') {
    return get('profile.wallet')
  }
  if (txt === 'air@drop.com') {
    return get('profile.email')
  }
  if (txt === '@user') {
    return get('profile.username')
  }
  if (txt === '1111111111') {
    return get('profile.phone')
  }
  return txt
}

web.execute = config => {
  const result = []
  config = typeof config === 'string' ? JSON.parse(
    config.split("'").join('"').replace(/\="([@\w\d]+)"/ig, "='$1'")
  ) : config
  const { url, tests } = config

  tests.map(test => {
    test.commands.map(cmd => {
      cmd.target = cmd.target.split('css=').join('')
      cmd.value = web.value(cmd.value)
    })
  })

  const template = fs.readFileSync(path.resolve(__dirname, './lib/webexec.js')).toString()
  const confstr = JSON.stringify(config)
  const res = template.replace('//CONF', `
    const config = ${confstr}
  `)
  const filename = '/tmp/' + (+new Date) + '.js'
  fs.writeFileSync(filename, res)
  proc.exec('casperjs --engine=slimerjs ' + filename)
}



module.exports = web