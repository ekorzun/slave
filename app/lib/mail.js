var Imap = require("imap");
var MailParser = require("mailparser").MailParser;
var Promise = require("bluebird");
Promise.longStackTraces();



module.exports = function(opts) {
  var imapConfig = {
    user: opts.user || 'adil119',
    password: opts.password || '9GLooZH9KjbBlWnuLkVX',
    host: opts.host || 'imap.yandex.ru',
    port: 993,
    tls: true
  };

  var imap = new Imap(imapConfig);
  Promise.promisifyAll(imap);

  imap.once("error", function (err) {
    opts.onError && opts.onError(err)
  })

  imap.once("ready", function execute() {
    imap.openBox("INBOX", false, function (err, mailBox) {
      if (err) {
        console.error(err);
        return;
      }
      imap.search(["UNSEEN"], function (err, results) {
        if (!results || !results.length) {
          console.log("No unread mails");
          imap.end();
          return;
        }

        /* mark as seen
        imap.setFlags(results, ['\\Seen'], function(err) {
            if (!err) {
                console.log("marked as read");
            } else {
                console.log(JSON.stringify(err, null, 2));
            }
        });*/

        var f = imap.fetch(results, { bodies: "" });


        f.on("message", function processMessage(msg, seqno) {
          console.log("Processing msg #" + seqno);
          // console.log(msg);

          var parser = new MailParser();
          parser.on("headers", function (headers) {
            console.log("Header: " + JSON.stringify(headers));
          });

          parser.on('data', data => {
            if (data.type === 'text') {
              // console.log(seqno);
              // console.log(data.text);  /* data.html*/
            }
          });

          msg.on("body", function (stream) {
            stream.on("data", function (chunk) {
              parser.write(chunk.toString("utf8"));
            });
          });
          msg.once("end", function () {
            // console.log("Finished msg #" + seqno);
            parser.end();
          });
        });


        f.once("error", function (err) {
          return Promise.reject(err);
        });
        f.once("end", function () {
          console.log("Done fetching all unseen messages.");
          imap.end();
        });
      });
    });
  })

  imap.connect()
}




