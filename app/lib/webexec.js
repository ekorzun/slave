//CONF
// const config = {"id":"3bcfb014-d09b-4912-9f16-b6693d56fcc0","name":"Untitled Project","url":"https://sociali.io","tests":[{"id":"23884882-cbfe-430f-9fca-20308c7eeb86","name":"Untitled","commands":[{"id":"80b6e279-5798-444f-be89-3a67be10732b","comment":"","command":"open","target":"/ref/Fu13108619","value":""},{"id":"97dff15e-dd23-45bc-816e-adb8174c1cb9","comment":"","command":"clickAt","target":"name=txt_name","value":"77,25"},{"id":"2ad8a091-8787-4659-8369-f5ec5cdef47b","comment":"","command":"type","target":"name=txt_name","value":"name"},{"id":"5ee74c17-8e10-45c0-a774-f75b81bf91fa","comment":"","command":"type","target":"name=txt_email","value":"air@drop.com"},{"id":"5120983f-68ff-4692-88bf-d5b1748cb3da","comment":"","command":"clickAt","target":"css=div.checkbox","value":"167,26"},{"id":"08a882f8-e77e-4d2f-afe5-a9c77fc75732","comment":"","command":"clickAt","target":"id=t_agree","value":"15,11"},{"id":"c50c6cc7-0aef-4a46-b3ea-ff8f73206d5d","comment":"","command":"clickAt","target":"css=#lead_button > p","value":"132,14"}]}],"suites":[{"id":"696b410c-a737-4c15-a30f-337e2375f0aa","name":"Default Suite","tests":["23884882-cbfe-430f-9fca-20308c7eeb86"]}],"urls":["https://sociali.io"]}


const casper = require('casper').create({
  engine: 'slimerjs',
  verbose: true,
  logLevel: 'debug'
})

// const url = casper.cli.get('url')
const url = (config.urls[0] + "" + config.tests[0].commands[0].target).replace(/\s+/g, '')

console.log("AUR!", url)

casper.start(url, function() {
  this.wait(5000)
})

config.tests[0].commands.forEach(function(cmd, i) {
  if(i === 0) {
    return
  }

  !function(cmd) {
    cmd.target = cmd.target.split('css=').join('')
    casper.then(function() {
      this.echo(JSON.stringify(cmd))
      this.wait(3000)

      if (/^[\w]+=.+$/.test(cmd.target)) {
        // this.echo('12121212211')
        cmd.target = '[' + cmd.target.split('=')[0] + '="' + cmd.target.split('=')[1] + '"]'
      }

      switch (cmd.command) {
        case 'type': {
          if(cmd.target.indexOf('//') > -1) {
            this.fillXPath('form', {
              [cmd.target]: cmd.value
            })
          } else {
            this.fillSelectors('form', {
              [cmd.target]: cmd.value
            })
          }
          break
        }

        case 'clickAt': {
          this.click(cmd.target.indexOf('//') > -1 ? {
            type: 'xpath',
            path: cmd.target
          } :cmd.target)
        }
      }
    })
  }(cmd)
})

casper.then(function() {
  this.wait(3000)
  this.capture('/root/slave/a.png', {
    top: 0,
    left: 0,
    width: 1000,
    height: 1500
  });
  this.evaulate(function(params) {
    console.log(document.getElementById('freebirdFormviewerViewResponsePageTitle').innerHTML)
  })
})

casper.run()