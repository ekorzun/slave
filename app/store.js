const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const conf = require('./conf')

const db = low(new FileSync(`${conf.root}/db.json`))

db.defaults({
  profile: {},
  facebook: {},
  twitter: {},
  data: {},
  cron: {},
  queue: [],
}).write()


const get = key => db.get(key).value()
const set = (key, val) => db.set(key, val).write()

module.exports = {
  db,
  set,
  get,
}